/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Http, Headers, RequestOptionsArgs, Response, URLSearchParams} from '@angular/http';
import {Injectable, Optional} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as models from '../model/models';
import 'rxjs/Rx';

/* tslint:disable:no-unused-variable member-ordering */

'use strict';

@Injectable()
export class BlockApi {
    protected basePath = 'http://betaapi.tezex.info/v2';
    public defaultHeaders : Headers = new Headers();

    constructor(protected http: Http, @Optional() basePath: string) {
        if (basePath) {
            this.basePath = basePath;
        }
    }

    /**
     * Get All Blocks 
     * Get all Blocks
     * @param page Pagination, 200 tx per page max
     * @param order ASC or DESC
     * @param limit Results per Page
     */
    public blocksAll (page?: number, order?: string, limit?: number, extraHttpRequestParams?: any ) : Observable<models.BlocksAll> {
        const path = this.basePath + '/blocks/all';

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        if (page !== undefined) {
            queryParameters.set('page', String(page));
        }

        if (order !== undefined) {
            queryParameters.set('order', String(order));
        }

        if (limit !== undefined) {
            queryParameters.set('limit', String(limit));
        }

        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Get All Blocks for a specific Level
     * Get all Blocks for a specific Level
     * @param level The level of the Blocks to retrieve, includes abandoned
     */
    public blocksByLevel (level: number, extraHttpRequestParams?: any ) : Observable<Array<models.Block>> {
        const path = this.basePath + '/blocks/{level}'
            .replace('{' + 'level' + '}', String(level));

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        // verify required parameter 'level' is not null or undefined
        if (level === null || level === undefined) {
            throw new Error('Required parameter level was null or undefined when calling blocksByLevel.');
        }
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Get All Blocks for a specific Level-Range
     * Get all Blocks for a specific Level-Range
     * @param startlevel lowest blocklevel to return
     * @param stoplevel highest blocklevel to return
     */
    public blocksByLevelRange (startlevel: number, stoplevel: number, extraHttpRequestParams?: any ) : Observable<models.BlockRange> {
        const path = this.basePath + '/blocks/{startlevel}/{stoplevel}'
            .replace('{' + 'startlevel' + '}', String(startlevel))
            .replace('{' + 'stoplevel' + '}', String(stoplevel));

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        // verify required parameter 'startlevel' is not null or undefined
        if (startlevel === null || startlevel === undefined) {
            throw new Error('Required parameter startlevel was null or undefined when calling blocksByLevelRange.');
        }
        // verify required parameter 'stoplevel' is not null or undefined
        if (stoplevel === null || stoplevel === undefined) {
            throw new Error('Required parameter stoplevel was null or undefined when calling blocksByLevelRange.');
        }
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Get Block By Blockhash
     * Get a block by its hash
     * @param blockhash The hash of the Block to retrieve
     */
    public getBlock (blockhash: string, extraHttpRequestParams?: any ) : Observable<models.Block> {
        const path = this.basePath + '/block/{blockhash}'
            .replace('{' + 'blockhash' + '}', String(blockhash));

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        // verify required parameter 'blockhash' is not null or undefined
        if (blockhash === null || blockhash === undefined) {
            throw new Error('Required parameter blockhash was null or undefined when calling getBlock.');
        }
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Get Delegations of a Block
     * Get all Delegations of a specific Block
     * @param blockhash Blockhash
     */
    public getBlockDelegations (blockhash: string, extraHttpRequestParams?: any ) : Observable<Array<models.Delegation>> {
        const path = this.basePath + '/block/{blockhash}/operations/delegations'
            .replace('{' + 'blockhash' + '}', String(blockhash));

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        // verify required parameter 'blockhash' is not null or undefined
        if (blockhash === null || blockhash === undefined) {
            throw new Error('Required parameter blockhash was null or undefined when calling getBlockDelegations.');
        }
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Get Endorsements of a Block
     * Get all Endorsements of a specific Block
     * @param blockhash Blockhash
     */
    public getBlockEndorsements (blockhash: string, extraHttpRequestParams?: any ) : Observable<Array<models.Endorsement>> {
        const path = this.basePath + '/block/{blockhash}/operations/endorsements'
            .replace('{' + 'blockhash' + '}', String(blockhash));

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        // verify required parameter 'blockhash' is not null or undefined
        if (blockhash === null || blockhash === undefined) {
            throw new Error('Required parameter blockhash was null or undefined when calling getBlockEndorsements.');
        }
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Get operations of a block, sorted
     * Get the maximum Level we have seen, Blocks at this level may become abandoned Blocks later on
     * @param blockhash The hash of the Block to retrieve
     */
    public getBlockOperationsSorted (blockhash: string, extraHttpRequestParams?: any ) : Observable<models.BlockOperationsSorted> {
        const path = this.basePath + '/block/{blockhash}/operations'
            .replace('{' + 'blockhash' + '}', String(blockhash));

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        // verify required parameter 'blockhash' is not null or undefined
        if (blockhash === null || blockhash === undefined) {
            throw new Error('Required parameter blockhash was null or undefined when calling getBlockOperationsSorted.');
        }
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Get Originations of a Block
     * Get all Originations of a spcific Block
     * @param blockhash Blockhash
     */
    public getBlockOriginations (blockhash: string, extraHttpRequestParams?: any ) : Observable<Array<models.Origination>> {
        const path = this.basePath + '/block/{blockhash}/operations/originations'
            .replace('{' + 'blockhash' + '}', String(blockhash));

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        // verify required parameter 'blockhash' is not null or undefined
        if (blockhash === null || blockhash === undefined) {
            throw new Error('Required parameter blockhash was null or undefined when calling getBlockOriginations.');
        }
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Get Transactions of Block
     * Get all Transactions of a spcific Block
     * @param blockhash Blockhash
     */
    public getBlockTransaction (blockhash: string, extraHttpRequestParams?: any ) : Observable<Array<models.Transaction>> {
        const path = this.basePath + '/block/{blockhash}/operations/transactions'
            .replace('{' + 'blockhash' + '}', String(blockhash));

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        // verify required parameter 'blockhash' is not null or undefined
        if (blockhash === null || blockhash === undefined) {
            throw new Error('Required parameter blockhash was null or undefined when calling getBlockTransaction.');
        }
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * returns the last 25 blocks
     * Get all Blocks for a specific Level
     */
    public recentBlocks (extraHttpRequestParams?: any ) : Observable<Array<models.Block>> {
        const path = this.basePath + '/blocks/recent';

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

}
