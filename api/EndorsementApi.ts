/**
 * TezosAPI
 * BETA Tezos API, this may change frequently
 *
 * OpenAPI spec version: 0.0.2
 * Contact: office@bitfly.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Http, Headers, RequestOptionsArgs, Response, URLSearchParams} from '@angular/http';
import {Injectable, Optional} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as models from '../model/models';
import 'rxjs/Rx';

/* tslint:disable:no-unused-variable member-ordering */

'use strict';

@Injectable()
export class EndorsementApi {
    protected basePath = 'http://betaapi.tezex.info/v2';
    public defaultHeaders : Headers = new Headers();

    constructor(protected http: Http, @Optional() basePath: string) {
        if (basePath) {
            this.basePath = basePath;
        }
    }

    /**
     * Get Endorsement
     * Get a specific Endorsement
     * @param endorsementHash The hash of the Endorsement to retrieve
     */
    public getEndorsement (endorsementHash: string, extraHttpRequestParams?: any ) : Observable<models.Endorsement> {
        const path = this.basePath + '/endorsement/{endorsement_hash}'
            .replace('{' + 'endorsement_hash' + '}', String(endorsementHash));

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        // verify required parameter 'endorsementHash' is not null or undefined
        if (endorsementHash === null || endorsementHash === undefined) {
            throw new Error('Required parameter endorsementHash was null or undefined when calling getEndorsement.');
        }
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Get Endorsement
     * Get a specific Endorsement
     * @param blockHash blockhash
     */
    public getEndorsementForBlock (blockHash: string, extraHttpRequestParams?: any ) : Observable<Array<models.Endorsement>> {
        const path = this.basePath + '/endorsement/for/{block_hash}'
            .replace('{' + 'block_hash' + '}', String(blockHash));

        let queryParameters = new URLSearchParams();
        let headerParams = this.defaultHeaders;
        // verify required parameter 'blockHash' is not null or undefined
        if (blockHash === null || blockHash === undefined) {
            throw new Error('Required parameter blockHash was null or undefined when calling getEndorsementForBlock.');
        }
        let requestOptions: RequestOptionsArgs = {
            method: 'GET',
            headers: headerParams,
            search: queryParameters
        };

        return this.http.request(path, requestOptions)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

}
